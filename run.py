from validations.main import process_data
import argparse


def run():
    parser = argparse.ArgumentParser(description='Local validation pipeline')
    parser.add_argument('-o', '--output_dir', type=str, metavar='', required=False,
                        # Change to the location where you would like to see the validation output
                        default = r"\\lonfs01\MIO-Team\Ongoing Work\Transaction Data\2. Validation and Mapping Info\output\kl",
                        #default="C:\\Users\\*\\*\\output_folder",
                        help='location for output files on surface pro, '
                             'preface location with r and encase path in double quotes')

    parser.add_argument('-j', '--json_file', type=str, metavar='', required=False,
                        # Use the relevant json files
                        # default="gas_orders.json",
                        # default="gas_trades.json",
                        # default="power_orders.json",
                        default="power_trades.json",
                        help='json_file that matches your data dictionary, single file')


    parser.add_argument('-f', '--file_path', type=str, metavar='', required=False,
                        # Change to the location where the raw data stores - it has to be a csv file!
                      # default=r"F:\Ongoing Work\Transaction Data\1. Data Submissions (original)\3. Marex\Data\2020 12 December\Broker 3 Power Orders August 2021.csv",
                        default=r"\\lonfs01\MIO-Team\Ongoing Work\Transaction Data\1. Data Submissions (original)\5. Tullett\2021 09 September\Version 1\Broker 5 Power Trades September 2021.csv",
                        help='location of folder containing files, '
                             'preface location with r and encase path in double quotes')

    args = parser.parse_args()

    output_dir = args.output_dir
    json_file = args.json_file
    file_path = args.file_path

    print('output_dir = ', output_dir, '\njson_file = ', json_file, '\nfile_path = ', file_path)

    process_data(file_path, output_dir, json_file)


if __name__ == '__main__':

    run()



