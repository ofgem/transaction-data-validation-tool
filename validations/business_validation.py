import pandas as pd
import numpy as np
from validations import data_pipeline
import re


class BusinessValidation(data_pipeline.DataPipeline):

    def calculate(self):
        business_validation_errors = []

        invalid_cases = pd.DataFrame([i for i in self.valid_cases()])

        for index, row in invalid_cases.iterrows():

            expected_entry = [row['column_valid_cases'] if row['column_valid_cases']!= [True, False] else ['Y', 'N']]

            error_column = [row['table_column']] * len(row['error_entry'])
            valid_cases = expected_entry * len(row['error_entry'])

            bv_error_df = pd.DataFrame({'table_column': error_column, 'error_entry': row['error_entry'],
                                     'excel_row': row['row_id'], 'accepted entries': valid_cases
                                     })

            business_validation_errors.append(bv_error_df.to_json())

        print('business validation check complete')

        return business_validation_errors if business_validation_errors else None

    def valid_values_for_column(self, column_name):

        return [x["enum"] for x in self.schema_all if str(x['name']) == column_name[0] and x['enum']]


    @staticmethod
    def enum_check(x, column_valid_cases):
        return np.logical_not(any(case in column_valid_cases[0] for case in [str(x).lower(), x]))

    @staticmethod
    def nullable_check(nullable_column):
        return True if nullable_column[0] == False else False

    @staticmethod
    def is_nan_check(x):
            return x is np.nan or x != x

    def valid_cases(self):

        for column in list(self.df):
            data_column = column
            #data_column = column.replace(" ", "_").lower()
            dict_column = [i['name'] for i in self.schema_all if re.search(data_column, i['name'])]
            nullable_columns = [i['name'] for i in self.schema_all if i["nullable"] == True]

            if not dict_column or dict_column == 'date':

                continue

            elif dict_column:

                column_valid_cases = self.valid_values_for_column(dict_column)
                nullable_column = [True if dict_column[0] in nullable_columns else False]

                for valid_cases in column_valid_cases:

                    if valid_cases is None:
                        continue
                    else:
                        self.df['error'] = self.df[column].apply(lambda x: self.enum_check(x, column_valid_cases)
                        if self.is_nan_check(x) == False else self.nullable_check(nullable_column))

                        if self.df['error'].any():
                            sample = {'table_column': column,
                                      'error_entry': list(self.df.loc[self.df.error == True][column]),
                                      'row_id': list(self.df.loc[self.df.error == True]['row_index']),
                                      'column_valid_cases':column_valid_cases[0]
                                      }
                            yield sample


if __name__ == "__main__":
    pass
