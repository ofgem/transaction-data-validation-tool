import pandas as pd
import os
import re


class Collect:

    def __init__(self, file_path):
        self.file_path = file_path

    def date_column_finder(self):
        try:
            df = pd.read_csv(self.file_path, encoding='utf8')
        except UnicodeDecodeError:
            df = pd.read_csv(self.file_path, encoding='cp1252')

        pattern = r'Date|date|TIMESTAMP'
        date_cols = [c for c in df.columns if re.search(pattern, c)]
        return date_cols

    def csv_reader(self):

        supplier_folder = os.path.split(self.file_path)

        file_name = os.path.basename(supplier_folder[1])

        file_type_split = os.path.split(supplier_folder[0])
        file_type = file_type_split[1]
        supplier = os.path.split(file_type_split[0])

        supplier_name = supplier[1]

        date_columns = self.date_column_finder()
        date_col_dict = {}
        for col in date_columns:
            date_col_dict[col] = str
        try:

            df = pd.read_csv(self.file_path, encoding='utf8', converters=date_col_dict)
        except UnicodeDecodeError:
            df = pd.read_csv(self.file_path, encoding='cp1252', converters=date_col_dict)

        no_of_customers = len(df)

        return df, supplier_name, file_name, file_type, no_of_customers


if __name__ == "__main__":
    pass
    # Data_pipeline = DataPipeline(output_dir, json_file, file_path)



