from validations import ingestion_validation
from validations import datatype_validation
from validations import unique_validation
from validations import business_validation
from validations import conditional_validation
from validations import output_generator
import glob
import time
from pathlib import Path
import os


def process_data(file_path, output_dir, json_file):
    path = Path(output_dir)
    if not path / "error_logs":
        os.makedirs

    error_log_path = path / "error_logs"

    path / "html_outputs"
    if not os.path.exists(error_log_path):
        os.makedirs(error_log_path)

    t1 = time.time()

    for data_file in glob.iglob(file_path):
        print('processing file')

        Ingestion_validation_local_pipeline = ingestion_validation.IngestionValidation(error_log_path, output_dir, json_file, data_file)
        ingestion_errors = Ingestion_validation_local_pipeline.calculate()

        Data_type_validation_local_pipeline = datatype_validation.DataTypeValidation(error_log_path, output_dir, json_file, data_file)
        data_type_errors, filename, total_rows = Data_type_validation_local_pipeline.calculate()

        Unique_validation_local_pipeline = unique_validation.UniqueValidation(error_log_path, output_dir,
                                                                                     json_file, data_file)
        unique_errors, filename, total_rows = Unique_validation_local_pipeline.calculate()

        Business_validation_local_pipeline = business_validation.BusinessValidation(error_log_path, output_dir, json_file, data_file)
        business_type_errors = Business_validation_local_pipeline.calculate()

        Conditional_validation_local_pipeline = conditional_validation.ConditionalValidation(error_log_path, output_dir, json_file,data_file)
        conditional_type_errors = Conditional_validation_local_pipeline.calculate()

        validations = [i for i in [ingestion_errors, data_type_errors, unique_errors, business_type_errors, conditional_type_errors] if i]
        if validations:
            Output_generator_local_pipeline = output_generator.OutputGenerator(error_log_path, output_dir,
                                                                                                 json_file, data_file)
            Output_generator_local_pipeline.output(filename, ingestion_errors, data_type_errors, unique_errors,
                                                   business_type_errors, conditional_type_errors)
        else:
            Output_generator_local_pipeline = output_generator.OutputGenerator(error_log_path, output_dir,
                                                                               json_file, data_file)
            Output_generator_local_pipeline.html_error_free_output_generator(filename)

        print('all checks complete')


    t = time.time()-t1
    print("SELFTIMED:", t)





