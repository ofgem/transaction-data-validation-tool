from validations.data_pipeline import DataPipeline
import pandas as pd
import numpy as np
from datetime import date, timedelta
import datetime
from dateutil import parser

import holidays
from pandas.tseries.offsets import CustomBusinessDay


class UniqueValidation(DataPipeline):

    def calculate(self):
        unique_errors = []

        if self.unique_id_checker():

            invalid_cases = pd.DataFrame([i for i in self.unique_id_checker()])
            invalid_date_cases = pd.DataFrame([i for i in self.business_day_checker()])
            invalid_range_cases = pd.DataFrame([i for i in self.range_checker()])


            for index, row in invalid_cases.iterrows():
                error_column = [row['table_column']] * len(row['error_entry'])
                dict_error = [row['error_type']] * len(row['error_entry'])
                error_entry = row['error_entry']
                excel_row = row['row_id']
                error_df = pd.DataFrame({
                    'table_column':error_column,
                    'error_entry':error_entry,
                    'excel_row':excel_row,
                    'error': dict_error})
                unique_errors.append(error_df.to_json())

            for index, row in invalid_date_cases.iterrows():

                table_column = row['table_column']
                error_entry = row['error_entry']
                row_id = row['row_id']
                error_type = row['error_type']
                error_df = pd.DataFrame({
                    'table_column': table_column,
                    'error_entry': error_entry,
                    'row_id': row_id,
                    'error_type': error_type})

                unique_errors.append(error_df.to_json())

            for index, row in invalid_range_cases.iterrows():
                error_column = [row['table_column']] * len(row['error_entry'])
                dict_error = [row['error_type']] * len(row['error_entry'])
                error_entry = row['error_entry']
                excel_row = row['row_id']
                error_df = pd.DataFrame({
                    'table_column':error_column,
                    'error_entry':error_entry,
                    'excel_row':excel_row,
                    'error': dict_error})
                unique_errors.append(error_df.to_json())

            print('unique check complete ')

        return unique_errors if unique_errors else None, self.file_name, self.no_of_customers

    def month_start(self, dt, d_years=0, d_months=0):
        y, m = dt.year + d_years, dt.month + d_months
        a, m = divmod(m - 1, 12)
        return date(y + 0, m + 1, 1)

    def month_end(self, dt):
        return self.month_start(dt, 0, 1) + timedelta(-1)

    def month_start_month_end(self, column):
        dates_ = column.astype(str).to_list()
        for date_ in dates_:
            try:
                d = datetime.datetime.strptime(date_, '%Y-%m-%d')
                return self.month_start(d), self.month_end(d)
            except ValueError:
                try:
                    d = datetime.datetime.strptime(date_, '%Y-%m-%d %H:%M:%S')
                    return self.month_start(d), self.month_end(d)
                except ValueError:
                    try:
                        d = datetime.datetime.strptime(date_, '%Y-%m-%d')
                        return self.month_start(d), self.month_end(d)
                    except ValueError:
                        try:
                            d = parser.parse(date_, dayfirst=True)
                            return self.month_start(d), self.month_end(d)
                        except ValueError:
                            pass



    def date_cond(self, column):
        for x in self.schema_all:
            if column == x["name"] and x.get('date_cond', None):
                return x.get('date_cond', None)
        return False

    def unique_cond(self, column):
        for x in self.schema_all:
            if column == x["name"] and x.get('unique_cond', None):
                return x.get('unique_cond', None)
        return False

    def range_cond(self, column):
        for x in self.schema_all:
            if column == x["name"] and x.get('range', None):
                return x.get('range', None)

        return None

    def business_days(self, column):
        date_c = pd.to_datetime(self.df[column], errors='coerce', dayfirst=True)
        date_min, date_max = self.month_start_month_end(date_c)
        uk_holidays = holidays.UnitedKingdom()
        uk_bd = CustomBusinessDay(calendar=uk_holidays)
        countries_list = ['Scotland', 'Northern Ireland', 'Wales']
        b_dates = pd.date_range(start=date_min, end=date_max, freq=uk_bd)
        b_dates_eng = b_dates.difference([x for x in b_dates if uk_holidays.get(x) and any(
            country in uk_holidays.get(x) for country in countries_list) == False])
        b_dates_eng= [x.date() for x in b_dates_eng]
        date_col = pd.to_datetime(self.df[column], errors='coerce', dayfirst=True )

        return [str(x) for x in b_dates_eng if x not in list(date_col.dt.date)]

    def unique_id_column(self, column):
        for x in self.schema_all:
            if column == x["name"] and x.get('unique', None):
                return x.get('unique', None)
        return False

    def unique_id_idx(self, column):
        self.df['duplicates'] = self.df.duplicated(column, keep=False)
        cols = ['duplicates']
        self.df['error'] = np.where(self.df[cols].eq(True).all(1), True, False)
        return self.df['error']

    def unique_id_cond_idx(self, column, cond_col, cond_col_value):
        self.df['duplicates'] = self.df.duplicated([column, cond_col], keep=False)
        self.df['cond_true'] = self.df[cond_col] == cond_col_value
        cols = ['duplicates', 'cond_true']
        self.df['error'] = np.where(self.df[cols].eq(True).all(1), True, False)
        return self.df['error']

    def range_check(self, range_col, range_values):
        range_column = self.df[range_col].replace( '[\$,)]','', regex=True ).replace( '[(]','-',   regex=True ).replace( '', 'NaN', regex=True ).astype(float)
        return range_column.between(range_values[0], range_values[1])


    def unique_id_checker(self):
        for column in self.df.columns:
            if self.unique_cond(column):
                unique_cond_col, unique_cond_col_val = self.unique_cond(column)
                self.df['error'] = self.unique_id_cond_idx(column, unique_cond_col, unique_cond_col_val)
                sample = {'table_column': column,
                          'error_entry': list(self.df.loc[self.df.error == True][column]),
                          'row_id': list(self.df.loc[self.df.error == True]['row_index']),
                          'error_type': 'values are not unique in this column where they should be based on entries in another column'
                          }
                yield sample

            elif self.unique_id_column(column):
                self.df['error'] = self.unique_id_idx(column)
                if self.df['error'].any():
                    sample = {'table_column': column,
                              'error_entry': list(self.df.loc[self.df.error == True][column]),
                              'row_id': list(self.df.loc[self.df.error == True]['row_index']),
                              'error_type':'values are not unique in this column where they should be'
                              }
                    yield sample

    def business_day_checker(self):
        for column in self.df.columns:

            if self.date_cond(column):
                missing_dates = self.business_days(column)
                if missing_dates:
                    sample = {'table_column': column,
                              'error_entry': missing_dates,
                              'row_id': 'NA',
                              'error_type': 'business dates were not found in your data'
                              }
                    yield sample

    def range_checker(self):
        for column in self.df.columns:
            if self.range_cond(column):
                range_values = self.range_cond(column)
                if range_values:
                    self.df['error'] = ~self.range_check(column, range_values)
                    if self.df['error'].any():
                        sample = {'table_column': column,
                                  'error_entry': list(self.df.loc[self.df.error == True][column]),
                                  'row_id': list(self.df.loc[self.df.error == True]['row_index']),
                                  'error_type': 'these values are not within the expected range'
                                  }
                        yield sample

    def checker(self):
        self.unique_id_checker()
        self.business_day_checker()

